Rails.application.routes.draw do
  get 'store/index'

  resources :users
  get 'manage/index'
  get 'manage/terms'
  root 'store#index'
  resources :charges
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
